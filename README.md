# 安装依赖
  ```
  $ npm i
  ```

# 启动服务
  ```
  $ npm start
  ```
# api说明:
  1、上传图片接口: http://localhost:3000/upload_image
  2、下载图片接口: http://localhost:3000/download_image
  3、上传excel接口: http://localhost:3000/upload_excel